# Twitter Stream Flume #

Twitter Stream Flume is a Java Jdevelopper project that collect twitter stream through Flume and save the results in HDFS.

## How do I get set up? ##

### Dependencies ###

The flume agent use a custom streamer that use Twitter4j 4.0.1

It is also necessary to add the streamer to Flume class path.

If you are using Oracle Big Data VM, `./setup.sh` will take care of everything for you.

### Flume configuration ###

To set-up Flume, you need to edit `flume.conf`. Multiple lines need to be updated :

* `TwitterAgent.sources.Twitter.consumerKey`: your twitter consumer key
* `TwitterAgent.sources.Twitter.consumerSecret`: your twitter consumer secret
* `TwitterAgent.sources.Twitter.accessToken`: your twitter access Token
* `TwitterAgent.sources.Twitter.accessTokenSecret`: your twitter token secret
* `TwitterAgent.sources.Twitter.keywords`: a list of keywords, comma separated, to filter stream (not case sensitive)
* `TwitterAgent.sources.Twitter.proxyUrl`: your proxy url (leave "null" if you don't have any)
* `TwitterAgent.sources.Twitter.proxyPort`: your proxy port
* `TwitterAgent.sinks.HDFS.hdfs.path`: path where you want to store tweets

## Starting streaming ##

To start streaming, `cd` in the directory where you placed `flume.conf` and run

`flume-ng agent -n TwitterAgent -f flume.conf -Dflume.root.logger=DEBUG,console`

Or simply run `./run-flume.sh`