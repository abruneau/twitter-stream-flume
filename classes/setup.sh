echo Removing old Twitter4j classes
sudo mv /usr/lib/flume-ng/lib/twitter4j-* ./Twitter4j\ 3.x/

echo Adding new Twitter4j classes
sudo cp Twitter4j\ 4.0.1/twitter4j-* /usr/lib/flume-ng/lib/

echo Adding flume project to flume directory
sudo cp target/twitter-1.0.jar /usr/lib/flume-ng/lib/