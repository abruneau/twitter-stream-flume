echo Removing twitter4j 4.0.1
sudo rm /usr/lib/flume-ng/lib/twitter4j-*

echo Moving back twitter4j 3.x
sudo chmod 644 Twitter4j\ 3.x/*
sudo cp Twitter4j\ 3.x/* /usr/lib/flume-ng/lib/

echo Removing twitter project from twitter directory
sudo rm /usr/lib/flume-ng/lib/twitter-1.0.jar
